/*
  Contains some of the begining branches of the story's arc. 
  As well as the information containing the version of the pill taken. 
  Changing anything inputed to lowercacse so that the if statements can 
  handle it.

*/

import java.util.Scanner;
import java.lang.String;

public class branches
{
    private static int pill = 0;
	//1 is red, 2 is blue and 3 is refused.
    
    public static void checkBranch(String[] args)
    {
      	Scanner keyin = new Scanner(System.in);
        String situation = keyin.nextLine();
	situation.toLowerCase();
	if(situation == "yes")
	{
	    System.out.println("Well very good. Then there is not much time to explain, you must act quickly. Take either the blue pill or the read pill! (Red/Blue/Refuse)");
	}
	situation = keyin.nextLine();
	situation.toLowerCase();
	if(situation == "red")
	    {
		pill += 1;
		System.out.println("You feel no diffirent, but when you look around, the man has left without a trace. Do you look around for him or continue on your way toward your friends house? (look/go)");
		situation = keyin.nextLine();
		situation.toLowerCase();
	    }
       	else if (situation == "blue")
	    {
		pill += 2;
		System.out.println("The world starts suddenly spinning moments after you take the pill. The man fades out of vision and it becomes hard to keep your eyes focused.");
	    }
	else
	    {
		System.out.println("You refuse the strange mans offer and procede toward your friends house unconcerned.");
	    }
	if(situation == "look")
	    {
		System.out.println("You look around the area, but he is nowhere to be seen. You are searching down a fairly dark alley when you suddenly faint.");
	    }
	else if ( situation == "blue" || situation == "go")
	    /* situation will still be "blue" so this will catch situation 
	       "go" and situation "blue"*/
	    {
		System.out.println("You decide that, whatever the case, you should still go to your friends house. However, on the way the sky starts to change colour and it becomes hard to walk.");
	    }
    }
}